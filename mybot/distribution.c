/* Copyright (C) 2010 Benjamin S Wolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Benjamin S Wolf <jokeserver@gmail.com>
 */

#include "debug.h"
#include "distribution.h"
#include "misc.h"
#include "orders.h"
#include "state.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

// approximately knapsack
static int dist_rec(request_t* want, int len, int* budget, int* alloc,
                    orders_t* now, int* now_len) {
    int i, j, id, t, ships, s, o;
    int ord[num_planets];
    int avail[num_planets];
    int sums[num_planets];
    int select[num_planets];
    int used[num_planets];
    int best[num_planets];
    int bval = -1, blen = 0;
    int val = want->value;
    int val2;
    int usable;
    orders_t orders[num_planets];
    closest_info* cl;
    id = want->planet_id;
    t = want->turns;
    ships = want->ships;
    o = 0;
    memset(used, 0, num_planets * sizeof(int));
    if (len == 1) {
        for (cl = closest[id] + 1; cl->distance < t; ++cl) {
            s = budget[cl->planet_id] - alloc[cl->planet_id];
            if (s > 0) {
                s = min(s, ships);
                ships -= s;
                used[cl->planet_id] = s;
                if (ships == 0) {
                    for (i = 0; i < num_planets; ++i) {
                        alloc[i] += used[i];
                    }
                    return val;
                }
            }
        }
        for (; cl->distance == t; ++cl) {
            s = budget[cl->planet_id] - alloc[cl->planet_id];
            if (s > 0) {
                s = min(s, ships);
                ships -= s;
                used[cl->planet_id] = s;
                orders[o].from = cl->planet_id;
                orders[o].to = id;
                orders[o].ships = s;
                ++o;
                if (ships == 0) {
                    for (i = 0; i < num_planets; ++i) {
                        alloc[i] += used[i];
                    }
                    memcpy(now + *now_len, orders, o * sizeof(orders_t));
                    *now_len += o;
                    return val;
                }
            }
        }
        // we haven't adjusted alloc or anything else, so we're good
        return 0;
    }
    assert(len > 1);
    assert(val > 0);
    usable = 0;
    for (cl = closest[id] + 1; cl->distance < t; ++cl) {
        s = budget[cl->planet_id] - alloc[cl->planet_id];
        if (s > 0) {
            avail[usable] = s;
            ord[usable] = cl->planet_id;
            ++usable;
        }
    }
    o = *now_len;
    if (usable > 0) {
        memset(sums, 0, usable * sizeof(int));
        memset(select, 0, usable * sizeof(int));
        s = 0;
        for (i = usable - 1; i >= 0; --i) {
            s += avail[i];
            sums[i] = s;
        }
        if (ships <= sums[0]) {
            i = 0;
            while (i >= 0) {
                // down
                for (; i < usable - 1; ++i) {
                    if (ships == sums[i]) {
                        select[i] = 1;
                        used[i] = avail[i];
                    } else if (select[i]) {
                        used[i] = min(ships, avail[i]);
                    } else {
                        used[i] = max(0, ships - sums[i + 1]);
                    }
                    ships -= used[i];
                    alloc[ord[i]] += used[i];
                }
                assert(ships <= avail[usable - 1]);
                used[usable - 1] = ships;
                alloc[ord[usable - 1]] += ships;

                // recurse
                val2 = dist_rec(want + 1, len - 1, budget, alloc,
                                now, now_len);
                if (val2 > bval) {
                    memcpy(best, used, usable * sizeof(int));
                    bval = val2;
                    if (blen > 0) {
                        memmove(now + o, now + o + blen,
                                (*now_len - o - blen) * sizeof(orders_t));
                    }
                    blen = *now_len - o - blen;
                }
                // either way, set the order length back to the correct one.
                *now_len = o + blen;

                // up
                alloc[ord[usable - 1]] -= ships;
                for (i = usable - 2; i >= 0; --i) {
                    alloc[ord[i]] -= used[i];
                    ships += used[i];
                    if (select[i]) {
                        select[i] = 0;
                    } else {
                        select[i] = 1;
                        break;
                    }
                }
                // if we "overflowed" past select[0], i is now -1
                // skip right back out if len is too high
                if (len > 7) {
                    for (; i >= 0; --i) {
                        alloc[ord[i]] -= used[i];
                        ships += used[i];
                    }
                }
            }
            // we guarantee by the ships <= sums[0] clause
            // that fulfilled the request at some point
            assert(bval >= 0);
            bval += val;
        }
    }
    // one level down *without fulfilling this request* requires that we
    // skip past all the requests for the same planet.
    for (i = 1; i < len; ++i) {
        if ((want + i)->planet_id != id) {
            val2 = dist_rec(want + i, len - i, budget, alloc, now, now_len);
            break;
        }
    }
    if (val2 > bval) {
        // move the orders from the "don't fulfill this request" case above
        // into place and adjust now_len
        if (blen > 0) {
            memmove(now + o, now + o + blen,
                    (*now_len - o - blen) * sizeof(orders_t));
        }
        *now_len -= blen;
        return val2;
    } else {
        // remove the orders from the "don't fulfill this request" case above
        *now_len = o + blen;
        assert(usable > 0);
        assert(bval > 0);
        // refill alloc as appropriate, and add any orders for this turn
        for (i = 0; i < usable; ++i) {
            alloc[ord[i]] += best[i];
            if (distance[id][ord[i]] == t) {
                now[*now_len].from = ord[i];
                now[*now_len].to = id;
                now[*now_len].ships = best[i];
                ++*now_len;
            }
        }
        return bval;
    }
    return 0;
}

int compare_request(const void* r1, const void* r2) {
    return (((request_t*)r1)->planet_id - ((request_t*)r2)->planet_id) << 8
           + ((request_t*)r1)->turns - ((request_t*)r2)->turns;
}

order_set* distribute_ships(request_t* want, int len, int* budget, int* alloc) {
    int i, olen, val;
    order_set* set;
    orders_t now[num_planets * num_planets];
    if (len <= 0) {
        return NULL;
    }
    olen = 0;
    qsort(want, len, sizeof(request_t), compare_request);
    val = dist_rec(want, len, budget, alloc, now, &olen);
    LOG(DBG_DISTRIBUTION, "Found a value of %d in the given requests.\n", val);
    LOG(DBG_DISTRIBUTION, "Packaging %d orders.\n", olen);
    if (olen > 0) {
        set = alloc_order_set(olen);
        memcpy(set->orders, now, olen * sizeof(orders_t));
        return set;
    }
    return NULL;
}
