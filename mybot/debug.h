/* Copyright (C) 2010 Benjamin S Wolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Benjamin S Wolf <jokeserver@gmail.com>
 */

/*
 * Debugging macros
 */

#ifndef PW_DEBUG_H
#define PW_DEBUG_H

#include <stdio.h>

#define DBG_READ_PLANETS  0x00000001
#define DBG_READ_FLEETS   0x00000002
#define DBG_TURN_START    0x00000004
#define DBG_CALC_DISTANCE 0x00000008
#define DBG_CALC_CLOSEST  0x00000010
#define DBG_ALLOC_STREAMS 0x00000020
#define DBG_SMOOTH_START  0x00000040
#define DBG_SMOOTH_STEP   0x00000080
#define DBG_GAME_STATS    0x00000100
#define DBG_TURN_ONE      0x00000200
#define DBG_TURN_END      0x00000400
#define DBG_KNAPSACK      0x00000800
#define DBG_AGGRESSION    0x00001000
#define DBG_DEFENSE       0x00002000
#define DBG_ATTACK        0x00004000
#define DBG_CONQUER       0x00008000
#define DBG_FRONTLINE     0x00010000
#define DBG_DISTRIBUTION  0x00020000
#define DBG_REINFORCE     0x00040000
#define DBG_SNIPE         0x00080000

#define DBG_INIT (DBG_CALC_DISTANCE | DBG_CALC_CLOSEST | DBG_ALLOC_STREAMS)
#define DBG_INPUT (DBG_READ_PLANETS | DBG_READ_FLEETS | DBG_TURN_START)
#define DBG_SMOOTH_STREAM (DBG_SMOOTH_START | DBG_SMOOTH_STEP)
#define DBG_MILITARY (DBG_AGGRESSION | DBG_DEFENSE | DBG_ATTACK \
                      | DBG_CONQUER | DBG_REINFORCE | DBG_SNIPE)

#ifndef NDEBUG
#define DEBUG (DBG_INPUT | DBG_GAME_STATS \
               | DBG_TURN_ONE | DBG_TURN_END | DBG_KNAPSACK | DBG_MILITARY \
               | DBG_FRONTLINE | DBG_DISTRIBUTION)
#endif

#ifdef DEBUG
    FILE* fp_;
#define OPEN_LOG(file, logmode) fp_ = fopen(file, logmode)
#define CLOSE_LOG() fclose(fp_)
#define LOG(LOG_TYPE, msg, ...) \
        { if ((DEBUG) & (LOG_TYPE)) { \
                fprintf(fp_, "%s ", #LOG_TYPE); \
                fprintf(fp_, msg, ## __VA_ARGS__); \
                fflush(fp_); } }
#else
#define OPEN_LOG(file, logmode)
#define LOG(LOG_TYPE, msg, ...)
#define CLOSE_LOG()
#endif

#endif
