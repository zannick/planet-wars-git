/* Copyright (C) 2010 Benjamin S Wolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Benjamin S Wolf <jokeserver@gmail.com>
 */

#include "orders.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Create an empty set of len orders.
inline order_set* alloc_order_set(int len) {
    order_set* set = malloc(sizeof(order_set));
    set->orders = malloc(len * sizeof(orders_t));
    set->len = len;
    return set;
}

// Combine multiple order_sets.
inline order_set* merge_order_sets(order_set** sets, int len) {
    int orderlen = 0, pos = 0;
    int i;
    order_set* t;
    for (i = 0; i < len; ++i) {
        if (sets[i]) {
            orderlen += sets[i]->len;
        }
    }
    t = alloc_order_set(orderlen);
    for (i = 0; i < len; ++i) {
        if (sets[i]) {
            memcpy(t->orders + pos, sets[i]->orders, sets[i]->len * sizeof(orders_t));
            pos += sets[i]->len;
            destroy_order_set(sets[i]);
        }
    }
    return t;
}

// Output the order
inline void issue_order(int from, int to, int ships) {
    printf("%d %d %d\n", from, to, ships);
}

inline void destroy_order_set(order_set* set) {
    if (set == NULL) return;
    free(set->orders);
    free(set);
}

inline void issue_order_t(orders_t* order) {
    printf("%d %d %d\n", order->from, order->to, order->ships);
}

// Output all orders.
// Doesn't update state.
inline void process_orders(order_set* set) {
    if (set == NULL) {
        return;
    }
    orders_t* iter;
    for (iter = set->orders; iter < set->orders + set->len; ++iter) {
        issue_order_t(iter);
    }
}

inline void end_turn() {
    printf("go\n");
    fflush(stdout);
}
