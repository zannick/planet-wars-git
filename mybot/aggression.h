/* Copyright (C) 2010 Benjamin S Wolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Benjamin S Wolf <jokeserver@gmail.com>
 */

/*
 * Contains functions for use in determining aggression and expansion.
 */
#ifndef PW_AGGRESSION_H
#define PW_AGGRESSION_H

#include "debug.h"
#include "orders.h"
#include "state.h"

// aggression levels
enum agg_level {
    AGG_DEFEND = -2,
    AGG_CAUTION,
    AGG_CONQUER,
    AGG_ATTACK,
    AGG_DESTROY,
};

extern enum agg_level aggression;

#define set_aggression_level(level) \
    LOG(DBG_AGGRESSION, "Setting aggression level to %s.\n", #level); \
    aggression = level

extern int* frontline;

// Determines which planets are "frontline" strictly by
// examination of the closest array.
void find_frontline();

// Creates a set of orders to enable defense against incoming planets.
// Budget should have num_planets elements.
order_set* defense(int* budget, int* alloc);

order_set* snipe(int* budget, int* alloc);

order_set* conquer(int* budget, int* alloc);

order_set* attack(int* budget, int* alloc);

order_set* reinforce(int* budget, int* alloc, order_set* atk, order_set* def,
                     order_set* conq, order_set* sniper);

#endif /* PW_AGGRESSION_H */
