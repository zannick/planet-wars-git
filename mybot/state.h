/* Copyright (C) 2010 Benjamin S Wolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Benjamin S Wolf <jokeserver@gmail.com>
 */

/*
 * Provide state definitions for the planet wars bot.
 */
#ifndef PW_STATE_H
#define PW_STATE_H

#include <stdlib.h>
#include "stream.h"

typedef struct planet_t {
    double x;
    double y;
    int owner;
    int ships;
    int growth;
} planet;

typedef struct fleet_t {
    int owner;
    int src;
    int dest;
    int ships;
    int time_remaining;
} fleet;

extern int numships_p1;
extern int numships_p2;
extern int growth_p1;
extern int growth_p2;

extern int turn;

extern int num_planets;
extern planet* planets;
// growth by planet
extern int* growth;
// owners by planet
extern int* owners;

extern int** distance;
// closest[x] is a list of these, in increasing order of distance
typedef struct closest_info_t {
    int planet_id;
    int distance;
} closest_info;

extern closest_info** closest;

/*typedef struct fleet_list_t {
    int size;
    int cap;
    fleet* fleets;
} fleet_list;

// by planet id. the lists should be kept sorted in inc. order of time left
extern fleet_list* incoming;*/

// list of streams, one per planet
extern stream_t* streams;

// Returns the id of the player closest to the given planet,
// or 0 if both are equidistant.
int closest_player(int planet);

/*
 * Reads stdin for the initial state and populates any data given
 */
void init();

/*
 * Reads stdin for the state of subsequent turns.
 */
void refresh_state();

#endif
