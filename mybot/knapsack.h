/* Copyright (C) 2010 Benjamin S Wolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Benjamin S Wolf <jokeserver@gmail.com>
 */

/*
 * Contains functions to calculate the best selection from a set of
 * options.
 */

#ifndef PW_KNAPSACK_H
#define PW_KNAPSACK_H

typedef struct item2 {
    int id; // for caller's benefit
    int value;
    int cost;
    int rebate;
} item2_t;

/*
 * Given a list of len items, each with a value, cost, and rebate, determines
 * a set of items that yields maximal value.
 * The rebates are used to find a better selection wherein you can
 * overspend your budget but regain back to it later. You still cannot
 * overspend past the max cost.
 * Returns the value obtained, and sets the entries of the get array to
 * 0 or 1 for each item, with 1 indicating the item is in the set.
 */
int knapsack_2d(item2_t* items, int len, int budget, int max_cost, int* get);

typedef struct item {
    int id; // for caller's benefit
    int value;
    int cost;
} item_t;

/*
 * Given a list of len items, each with a value and a cost, determines a set
 * of items that yields maximal value.
 * Returns the value obtained, and sets the entries of the get array to
 * 0 or 1 for each item, with 1 indicating the item is in the set.
 */
int knapsack(item_t* items, int len, int budget, int* get);

#endif /* PW_KNAPSACK_H */
