/* Copyright (C) 2010 Benjamin S Wolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Benjamin S Wolf <jokeserver@gmail.com>
 */

#include "aggression.h"
#include "debug.h"
#include "distribution.h"
#include "knapsack.h"
#include "orders.h"
#include "state.h"
#include "stream.h"
#include <assert.h>
#include <string.h>

enum agg_level aggression;

int* frontline = NULL;

void find_frontline() {
    int i, j, k, m, o, dist, p;
    int friendlys[num_planets - 1];
    closest_info* cl;
    if (frontline == NULL) {
        frontline = malloc(num_planets * sizeof(int));
    }
    memset(frontline, 0, num_planets * sizeof(int));
    o = STREAM_MIN_INDEX(streams[0]).owner;
    if (growth_p1 == 0 && o != 1 || growth_p2 == 0 && o != 2) {
        // a player has no planets
        LOG(DBG_FRONTLINE, "No planets, no frontline.\n");
        return;
    }
    // A planet is on the frontline if:
    // 1) among all friendly planets nearer to it than its nearest enemy neighbor,
    //    it is the closet
    // or 2) it is the nearest enemy neighbor of some enemy planet.
    for (i = 0; i < num_planets; ++i) {
        if (frontline[i]) {
            continue;
        }
        p = STREAM_MIN_INDEX(streams[i]).owner;
        if (p == 0) {
            continue;
        }
        j = 0;
        for (cl = closest[i] + 1; cl < closest[i] + num_planets; ++cl) {
            m = cl->planet_id;
            o = STREAM_MIN_INDEX(streams[m]).owner;
            if (o == p) {
                friendlys[j] = m;
                ++j;
            } else if (o == 3 - p) {
                // found the enemy
                dist = cl->distance;
                frontline[m] = 1;
                for (; cl < closest[i] + num_planets && cl->distance <= dist; ++cl) {
                    k = cl->planet_id;
                    o = STREAM_MIN_INDEX(streams[k]).owner;
                    if (o == p) {
                        friendlys[j] = k;
                        ++j;
                    } else if (o == 3 - p) {
                        frontline[k] = 1;
                    }
                }
                break;
            }
        }
        // There are j friendly planets closer than the enemy.
        // If any of them are closer *to* the enemy, this is not frontline.
        for (k = 0; k < j; ++k) {
            if (distance[m][friendlys[k]] < dist) {
                break;
            }
        }
        if (k == j) {
            // Never broke out of the loop
            frontline[i] = 1;
        }
    }
#ifdef DEBUG
    for (i = 0; i < num_planets; ++i) {
        if (frontline[i]) {
            LOG(DBG_FRONTLINE, "Planet %d is a frontline planet.\n", i);
        }
    }
#endif
}

order_set* defense(int* budget, int* alloc) {
    int i, t, m, ships, from, to;
    stream_buffer_t* buf;
    order_set* set = NULL;
    //int inc[num_planets];
    //int dbudget[num_planets];
    request_t reqs[num_planets * num_planets];
    int num_reqs = 0;
    //memcpy(dbudget, budget, num_planets * sizeof(int));
    // To determine if a planet I control is under attack, see if
    // it belongs to the opponent at the end of the stream. In fact, just
    // check to see if it belongs to the opponent at the end, then see
    // if I ever control it before then.
    for (i = 0; i < num_planets; ++i) {
        if (STREAM_MAX_INDEX(streams[i]).owner != 2) {
            continue;
        }
        //memset(inc, 0, num_planets * sizeof(int));
        t = 0;
        m = 0;
        FOR_STREAM(buf, streams[i]) {
            switch(m) {
            case 0:
                if (buf->owner == 1) {
                    m = 1;
                }
                break;
            case 1:
                if (buf->owner != 1) {
                    m = 2;
                    ships = buf->ships;
                    //budget[i] = 0;
                    reqs[num_reqs].planet_id = i;
                    reqs[num_reqs].turns = t;
                    reqs[num_reqs].ships = ships;
                    reqs[num_reqs].value = max(1, 2 * growth[i]);
                    ++num_reqs;
                }
                break;
            case 2:
            default:
                ++m;
                // also equal to ships = buf->ships - ships - growth[i]
                //ships = buf->inc_ships_p2 - buf->inc_ships_p1;
                if (buf->owner != 1) {
                    // number of ships they gained that was not growth
                    // ships = buf->ships - ships - growth[i];
                    ships = buf->inc_ships_p2 - buf->inc_ships_p1;
                    if (ships > 0) {
                        reqs[num_reqs].planet_id = i;
                        reqs[num_reqs].turns = t;
                        reqs[num_reqs].ships = ships;
                        reqs[num_reqs].value = max(1, 2 * growth[i]);
                        ++num_reqs;
                    } else {
                        reqs[num_reqs - 1].value += 2 * growth[i];
                    }
                } else {
                    m = 1;
                }
                break;
            }
            ++t;
        }
        if (m >= 2) {
            // we have created a request for this planet
            // set the value of the last request to be the number
            // of ships gained to the next turn that's a multiple of 50
            t = reqs[num_reqs - 1].turns;
            reqs[num_reqs - 1].value =
                max(1, 2 * growth[i] * (50 - ((turn + t) % 50)));
        }
    }
    LOG(DBG_DEFENSE, "Generated %d requests for defense.\n", num_reqs);
    set = distribute_ships(reqs, num_reqs, budget, alloc);
    LOG_ORDERS(DEFENSE, set);
    return set;
}

// Assumes id is never controlled by p in its stream.
// Returns -1 if can't conquer with current budget/setup.
int time_to_conquer(int id, int p, int* budget) {
    int ships = 0, g = 0, turns = 0;
    closest_info* cl;
    // Don't let the nested for loop deceive you, this is linear in max distance.
    for (cl = closest[id] + 1; cl < closest[id] + num_planets; ++cl) {
        if (STREAM_MIN_INDEX(streams[cl->planet_id]).owner == p) {
            // Assumes growth is spendable here.
            for (; g > 0 && turns < cl->distance; ++turns) {
                ships += g;
                if (ships > STREAM_INDEX(streams[id], turns).ships) {
                    return turns;
                }
            }
            ships += budget[cl->planet_id];
            if (ships > STREAM_INDEX(streams[id], turns).ships) {
                return turns;
            }
            g += growth[cl->planet_id];
        }
    }
    return -1;
}

// To determine which neutral planets, if any, we wish to conquer,
// we check for one of several conditions.
//   Is it closer to us than any enemy planet?
//   Does the enemy have the opposite planet or will they in the future?
//   Is the enemy attacking this planet (and can we reach it the turn after?)
// The last condition here is known as "sniping", and is handled here. The
// other two are handled in conquer().
order_set* snipe(int* budget, int* alloc) {
    int i, t, ships;
    stream_buffer_t* buf;
    order_set* snipe_set = NULL;
    request_t reqs[num_planets];
    int num_reqs = 0;
    for (i = 0; i < num_planets; ++i) {
        if (STREAM_MIN_INDEX(streams[i]).owner != 0) {
            continue;
        }
        if (STREAM_MAX_INDEX(streams[i]).owner != 2) {
            // either still neutral or will be ours, handled elsewhere
            continue;
        }
        if (closest_player(i) == 2) {
            // not worth considering these
            continue;
        }
        t = 1;
        FOR_STREAM_FROM(buf, 1, streams[i]) {
            if (buf->owner == 2) {
                // Snipe
                // Just make the request, doesn't matter if we can't make it
                reqs[num_reqs].planet_id = i;
                ships = STREAM_INDEX(streams[i], t-1).ships;
                if (ships >= growth[i]) {
                    if (t < streams[i].length - 1) {
                        if (STREAM_INDEX(streams[i], t + 1).owner == 1) {
                            //t = 0;
                            break;
                        }
                        ships = STREAM_INDEX(streams[i], t + 1).ships + 1;
                    } else {
                        ships = buf->ships + growth[i] + 1;
                    }
                    reqs[num_reqs].ships = ships;
                    reqs[num_reqs].turns = t + 1;
                    reqs[num_reqs].value =
                            max(1, 2 * growth[i] * (50 - ((turn + t+1) % 50)));
                } else {
                    reqs[num_reqs].ships = buf->inc_ships_p2 - buf->inc_ships_p1 + 1;
                    reqs[num_reqs].turns = t;
                    reqs[num_reqs].value =
                            max(1, 2 * growth[i] * (50 - ((turn + t) % 50)));
                }
                ++num_reqs;
                //t = 0;
                break;
            }
            ++t;
        }
    }
    LOG(DBG_SNIPE, "Generated %d requests for sniping.\n", num_reqs);
    snipe_set = distribute_ships(reqs, num_reqs, budget, alloc);
    LOG(DBG_SNIPE, "Snipe orders:\n");
    LOG_ORDERS(SNIPE, snipe_set);
    return snipe_set;
}

int compare_items(const void* a, const void* b) {
    return ((item_t*)a)->value - ((item_t*)a)->cost
         - ((item_t*)b)->value + ((item_t*)b)->cost;
}

order_set* conquer(int* budget, int* alloc) {
    int i, num = 0, ships = 0, nships, t;
    int j, n, u, num_sets;
    item_t want[num_planets];
    int get[num_planets];
    int used[num_planets];
    closest_info* cl;
    order_set* tset = NULL;
    for (i = 0; i < num_planets; ++i) {
        if (STREAM_MIN_INDEX(streams[i]).owner != 0) {
            continue;
        }
        if (STREAM_MAX_INDEX(streams[i]).owner != 0) {
            // handled elsewhere
            continue;
        }
        // with those two checks, it is therefore the case that this planet
        // is always neutral within the stream.
        switch(closest_player(i)) {
        case 0:
            // equidistant
            t = 0;
            for (cl = closest[i] + 1; cl < closest[i] + num_planets; ++cl) {
                if (t == 1) {
                    if (STREAM_MIN_INDEX(streams[cl->planet_id]).owner == 1) {
                        t = cl->distance;
                        ships += budget[cl->planet_id] - alloc[cl->planet_id];
                        nships = STREAM_INDEX(streams[i], t).ships;
                        if (ships > 0 && ships > nships) {
                            want[num].id = i;
                            want[num].cost = ships; //auto frontline
                            // add a bonus for being so contested
                            want[num].value = (1 + growth[i])
                                            * (50 - ((turn + t) % 50));
                            ++num;
                        }
                        break;
                    }
                } else if (t == 2) {
                    if (STREAM_MIN_INDEX(streams[cl->planet_id]).owner == 2) {
                        t = cl->distance;
                        ships -= STREAM_MIN_INDEX(streams[cl->planet_id]).ships;
                        ships -= growth[cl->planet_id];
                        nships = STREAM_INDEX(streams[i], t).ships;
                        if (ships > 0 && ships > nships) {
                            want[num].id = i;
                            want[num].cost = ships; //auto frontline
                            // add a bonus for being so contested
                            want[num].value = (1 + growth[i])
                                            * (50 - ((turn + t) % 50));
                            ++num;
                        }
                        break;
                    }
                } else {
                    // these breaks only break out of the switch
                    switch(STREAM_MIN_INDEX(streams[cl->planet_id]).owner) {
                    case 1:
                        ships += budget[cl->planet_id] - alloc[cl->planet_id];
                        // flag which player we want to see next
                        t = 2;
                        break;
                    case 2:
                        ships -= STREAM_MIN_INDEX(streams[cl->planet_id]).ships;
                        ships -= growth[cl->planet_id];
                        t = 1;
                        break;
                    default:
                        break;
                    }
                }
            }
            break;
        case 1:
            // simply calc if it's possible
            ships = 0;
            for (cl = closest[i] + 1; cl < closest[i] + num_planets; ++cl) {
                if (budget[cl->planet_id] > alloc[cl->planet_id]) {
                    t = cl->distance;
                    ships += budget[cl->planet_id] - alloc[cl->planet_id];
                    if (ships > STREAM_INDEX(streams[i], t).ships) {
                        ships = STREAM_INDEX(streams[i], t).ships + 1;
                        want[num].id = i;
                        want[num].cost = ships;
                        want[num].value = max(1,growth[i] * (50 - ((turn + t) % 50)));
                        ++num;
                        break;
                    }
                }
            }
            break;
        case 2:
        default:
            // not worth considering
            break;
        }
    }
    LOG(DBG_CONQUER, "Found %d possible targets for conquering.\n", num);
    if (num == 0) {
        return NULL;
    }
    qsort(want, num, sizeof(item_t), &compare_items);
    ships = 0;
    for (i = 0; i < num_planets; ++i) {
        ships += max(0, budget[i] - alloc[i]);
    }
    knapsack(want, num, ships, get);
    t = 0;
    for (i = 0; i < num; ++i) {
        if (get[i]) {
            ++t;
        }
    }
    if (t > 0) {
        order_set* sets[t];
        LOG(DBG_CONQUER, "Chose %d planets on first pass.\n", t);
        num_sets = 0;
        for (i = 0; i < num; ++i) {
            if (get[i]) {
                ships = 0;
                n = want[i].id;
                u = 0;
                t = 0;
                memset(used, 0, num_planets * sizeof(int));
                for (cl = closest[n] + 1; cl < closest[n] + num_planets; ++cl) {
                    nships = budget[cl->planet_id] - alloc[cl->planet_id];
                    if (nships > 0) {
                        if (cl->distance > t) {
                            t = cl->distance;
                            LOG(DBG_CONQUER, "Distance now %d.\n", t);
                            u = 0;
                        }
                        if (ships + nships > STREAM_INDEX(streams[n],
                                                          cl->distance).ships) {
                            used[cl->planet_id] = max(1, 
                                STREAM_INDEX(streams[n], cl->distance).ships
                                + 1 - ships);
                            ++u;
                            assert(distance[n][cl->planet_id] == t);
                            LOG(DBG_CONQUER, "%d total at %d dist (%d).\n",
                                             u, t, cl->planet_id);
                            sets[num_sets] = alloc_order_set(u);
                            u = 0;
                            for (j = 0; j < num_planets; ++j) {
                                if (used[j] > 0) {
                                    alloc[j] += used[j];
                                    LOG(DBG_CONQUER, "Used %d from %d (dist %d)\n",
                                        used[j], j, distance[n][j]);
                                    if (distance[n][j] == t) {
                                        assert(u < sets[num_sets]->len);
                                        sets[num_sets]->orders[u].from = j;
                                        sets[num_sets]->orders[u].to = n;
                                        sets[num_sets]->orders[u].ships = used[j];
                                        ++u;
                                    }
                                }
                            }
                            assert(u == sets[num_sets]->len);
                            ++num_sets;
                            break;
                        } else {
                            used[cl->planet_id] = nships;
                            ships += nships;
                            ++u;
                            LOG(DBG_CONQUER, "%d so far at %d dist (%d).\n",
                                             u, t, cl->planet_id);
                        }
                    }
                }
            }
        }
        LOG(DBG_CONQUER, "Accomplished %d.\n", num_sets);
        tset = merge_order_sets(sets, num_sets);
    }
    LOG(DBG_CONQUER, "Conquer orders:\n");
    LOG_ORDERS(CONQUER, tset);
    return tset;
}

// Number of turns before we want our ships to land.
static int* attack_plans = NULL;

order_set* attack(int* budget, int* alloc) {
    int i, j, t, num_reqs = 0;
    int ships, nships, g, c;
    closest_info* cl;
    order_set* sets[2];
    int cluster[num_planets];
    int get[num_planets];
    item_t want[num_planets];
    request_t reqs[num_planets];
    if (attack_plans == NULL) {
        attack_plans = calloc(num_planets, sizeof(int));
    }
    for (i = 0; i < num_planets; ++i) {
        // if it isn't continuously held by the opponent, it isn't done here
        if (STREAM_MIN_INDEX(streams[i]).owner != 2) {
            continue;
        }
        if (STREAM_MAX_INDEX(streams[i]).owner != 2) {
            continue;
        }
        if (attack_plans[i] > 0) {
            attack_plans[i] -= 1;
        }
        t = attack_plans[i];
        if (t > 0) {
            reqs[num_reqs].planet_id = i;
            reqs[num_reqs].turns = t;
            reqs[num_reqs].ships = STREAM_INDEX(streams[i], t).ships + 1;
            reqs[num_reqs].value = max(1, 2 * growth[i] * (50 - ((turn + t) % 50)));
            ++num_reqs;
        }
    }
    LOG(DBG_ATTACK, "Generated %d requests for existing attacks.\n", num_reqs);
    sets[0] = distribute_ships(reqs, num_reqs, budget, alloc);
    LOG_ORDERS(ATTACK, sets[0]);
    num_reqs = 0;
    for (i = 0; i < num_planets; ++i) {
        // if it isn't continuously held by the opponent, it isn't done here
        if (STREAM_MIN_INDEX(streams[i]).owner != 2) {
            continue;
        }
        if (STREAM_MAX_INDEX(streams[i]).owner != 2) {
            continue;
        }
        if (attack_plans[i] > 0) {
            continue;
        }
        if (frontline[i]) {
            g = 0;
            c = 0;
            for (cl = closest[i] + 1; cl < closest[i] + num_planets; ++cl) {
                cluster[c++] = cl->planet_id;
                ships = 0;
                nships = 0;
                for (j = 0; j < c; ++j) {
                    t = cl->distance - distance[i][cluster[j]];
                    if (STREAM_INDEX(streams[cluster[j]], t).owner == 1) {
                        ships += max(0, STREAM_INDEX(streams[cluster[j]], t).ships
                                        - alloc[cluster[j]]);
                    } else if (STREAM_INDEX(streams[cluster[j]], t).owner == 2) {
                        nships += STREAM_INDEX(streams[cluster[j]], t).ships;
                    }
                }
                t = cl->distance;
                if (ships - nships > STREAM_INDEX(streams[i], t).ships) {
                    // Can take
                    want[num_reqs].id = i;
                    LOG(DBG_ATTACK, "Can take with %d ships in %d turns "
                                    "(vs %d with %d reinf).\n",
                                    ships, t,
                                    STREAM_INDEX(streams[i], t).ships, nships);
                    ships = STREAM_INDEX(streams[i], t).ships + nships + 1;
                    want[num_reqs].cost = ships;
                    want[num_reqs].value = max(1,
                                    2 * growth[i] * (50 - ((turn + t) % 50)));
                    reqs[num_reqs].planet_id = i;
                    reqs[num_reqs].turns = t;
                    reqs[num_reqs].ships = ships;
                    reqs[num_reqs].value = want[num_reqs].value;
                    ++num_reqs;
                    break;
                }
            }
        }
    }
    LOG(DBG_ATTACK, "Found %d new targets.\n", num_reqs);
    if (num_reqs == 0) {
        return sets[0];
    }
    qsort(want, num_reqs, sizeof(item_t), &compare_items);
    ships = 0;
    for (i = 0; i < num_planets; ++i) {
        ships += max(0, budget[i] - alloc[i]);
    }
    knapsack(want, num_reqs, ships, get);
    t = 0;
    for (i = 0; i < num_reqs; ++i) {
        if (get[i]) {
            if (t != i) {
                memcpy(reqs + t, reqs + i, sizeof(request_t));
            }
            attack_plans[reqs[i].planet_id] = reqs[i].turns;
            ++t;
        }
    }
    LOG(DBG_ATTACK, "Picked %d targets.\n", t);
    sets[1] = distribute_ships(reqs, t, budget, alloc);
    LOG_ORDERS(ATTACK, sets[1]);
    if (sets[0] && sets[1]) {
        return merge_order_sets(sets, 2);
    } else if (sets[0] == NULL) {
        return sets[1];
    }
    return sets[0];
}

order_set* reinforce(int* budget, int* alloc, order_set* atk, order_set* def,
                     order_set* conq, order_set* sniper) {
    int i, j, k, ships;
    int o = 0, o2 = 0, lt;
    closest_info* cl;
    order_set* set;
    item_t to[num_planets];
    orders_t supply[num_planets * num_planets];
    // Allocate the remaining forces
    for (i = 0; i < num_planets; ++i) {
        ships = budget[i] - alloc[i];
        if (ships > 0) {
            lt = 0;
            LOG(DBG_REINFORCE, "Found %d ships available on planet %d.\n", ships, i);
            // Check whether we just sent any ships from this planet, if so
            // add them. Don't check for def this way.
            if (atk) {
                for (j = 0; j < atk->len; ++j) {
                    if (atk->orders[j].from == i) {
                        k = atk->orders[j].to;
                        //LOG(DBG_REINFORCE, "Fleet attacking planet %d.\n", k);
                        to[lt].id = k;
                        to[lt].cost = 2 * growth[i] - distance[0][i];
                        to[lt].value = 3 * growth[k] - distance[0][k];
                        ++lt;
                    }
                }
            }
            if (conq) {
                for (j = 0; j < conq->len; ++j) {
                    if (conq->orders[j].from == i) {
                        k = conq->orders[j].to;
                        //LOG(DBG_REINFORCE, "Fleet conquering planet %d.\n", k);
                        to[lt].id = k;
                        to[lt].cost = 2 * growth[i] - distance[0][i];
                        to[lt].value = 3 * growth[k] - distance[0][k];
                        ++lt;
                    }
                }
            }
            if (sniper) {
                for (j = 0; j < sniper->len; ++j) {
                    if (sniper->orders[j].from == i) {
                        k = sniper->orders[j].to;
                        //LOG(DBG_REINFORCE, "Fleet sniping planet %d.\n", k);
                        to[lt].id = k;
                        to[lt].cost = 2 * growth[i] - distance[0][i];
                        to[lt].value = 3 * growth[k] - distance[0][k];
                        ++lt;
                    }
                }
            }
            if (lt > 0) {
                qsort(to, lt, sizeof(item_t), &compare_items);
                for (j = 0; j < lt && ships > 0; ++j) {
                    if (to[j].value - to[j].cost > 0) {
                        supply[o].from = i;
                        supply[o].to = to[j].id;
                        if (ships == 1) {
                            supply[o].ships = 1;
                            ships = 0;
                            ++o;
                            break;
                        } else {
                            k = (j == lt - 1) ? ships : ships / 2;
                            supply[o].ships = k;
                            ships -= k;
                            if (k > 0) {
                                ++o;
                            }
                        }
                    }
                }
            }
            LOG(DBG_REINFORCE, "%d ships left for frontline.\n", ships);
            if (ships > 0 && !frontline[i]) {
                for (cl = closest[i] + 1; cl < closest[i] + num_planets; ++cl) {
                    if (frontline[cl->planet_id]
                        && STREAM_MIN_INDEX(streams[cl->planet_id]).owner == 1) {
                        supply[o].from = i;
                        supply[o].to = cl->planet_id;
                        supply[o].ships = ships;
                        ++o;
                        break;
                    }
                }
            }
            LOG(DBG_REINFORCE, "Reinforcing from planet %d with %d orders.\n",
                               i, o - o2);
            o2 = o;
        }
    }
    LOG(DBG_REINFORCE, "Total of %d reinforcement orders.\n", o);
    set = alloc_order_set(o);
    memcpy(set->orders, supply, o * sizeof(orders_t));
    LOG_ORDERS(REINFORCE, set);
    return set;
}
