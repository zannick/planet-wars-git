/* Copyright (C) 2010 Benjamin S Wolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Benjamin S Wolf <jokeserver@gmail.com>
 */

/*
 * Defines streams and related functions.
 */
#ifndef PLANETWARS_STREAM_H_
#define PLANETWARS_STREAM_H_

#include "debug.h"
#include <stddef.h>
#include <stdlib.h>

typedef struct stream_buffer_t {
    int inc_ships_p1;
    int inc_ships_p2;
    // This is the state immediately after the above ships land
    int ships;
    int owner;
} stream_buffer_t;

typedef struct stream_t {
    int length;
    // Marks where the "current" turn is, 0 to length-1.
    int cur_index;
    // array[cur_index] is the current turn
    stream_buffer_t* array;
} stream_t;

#define STREAM_INDEX(stream, index) \
    (stream).array[((stream).cur_index + (index) >= (stream).length) ? \
                    (stream).cur_index + (index) - (stream).length \
                  : (stream).cur_index + (index)]

#define STREAM_MIN_INDEX(stream) \
    (stream).array[(stream).cur_index]

#define STREAM_MAX_INDEX(stream) \
    (stream).array[((stream).cur_index == 0) ? \
                    (stream).length - 1 \
                  : (stream).cur_index - 1]

// Usage:
//  stream_buffer_t* buf;
//  FOR_STREAM(buf, stream) {
//    // buf starts at this turn's buffer, then increases by one for each turn
//    // use buf->element, etc.
//  }
#define FOR_STREAM(buf, stream) \
  for(buf = (stream).array + (stream).cur_index; \
      buf != NULL; \
      buf = (buf == (stream).array + \
                    ((stream).cur_index == 0 ? (stream).length - 1 \
                                             : (stream).cur_index - 1)) \
          ? NULL \
          : (buf == (stream).array + (stream).length - 1) ? (stream).array : buf + 1)

// Similar to FOR_STREAM above,
// but starts at a future turn (n >= 1) and iterates forward.
// Does not hit any turn before n, and if n is 0, will not iterate at all.
#define FOR_STREAM_FROM(buf, n, stream) \
  for(buf = (stream).array + (((stream).cur_index + (n) >= (stream).length) ? \
            (stream).cur_index + (n) - (stream).length : (stream).cur_index + n); \
      buf != (stream).array + (stream).cur_index; \
      buf = (buf == (stream).array + (stream).length - 1) ? (stream).array : buf + 1)

// Advance the stream forward one turn
// Doesn't clear the data
#define ITERATE_STREAM(stream) \
    (stream).cur_index = ((stream).cur_index == (stream).length - 1) ? 0 \
                        : (stream).cur_index + 1

#ifdef DEBUG
#define LOG_STREAM(flag, stream, i) { \
    stream_buffer_t* buf; \
    LOG(DBG_ ## flag, "Stream %d (%d long):\n", i, (stream).length); \
    FOR_STREAM(buf, stream) { \
        LOG(DBG_ ## flag, "  owner: %d ships: %d p1: %d p2: %d\n", \
            buf->owner, buf->ships, buf->inc_ships_p1, buf->inc_ships_p2); \
    } \
}
#else
#define LOG_STREAM(flag, stream, i)
#endif


#endif  /* PLANETWARS_STREAM_H_ */
