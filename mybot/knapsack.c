/* Copyright (C) 2010 Benjamin S Wolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Benjamin S Wolf <jokeserver@gmail.com>
 */

#include "knapsack.h"
#include "debug.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

int knapsack_2d(item2_t* items, int len, int budget, int max_cost, int* get) {
    int get2[len - 1];
    int c, v1, v2;
    if (budget <= 0 || max_cost <= 0) {
        memset(get, 0, len * sizeof(int));
        return 0;
    }
    c = items->cost - items->rebate;
    if (len == 1) {
        if (budget >= c && max_cost >= items->cost) {
            *get = 1;
            return items->value;
        } else {
            *get = 0;
            return 0;
        }
    }
    assert(len > 1);
    if (budget >= c && max_cost >= items->cost) {
        v1 = knapsack_2d(items + 1, len - 1, budget - c,
                         max_cost - items->cost, get + 1);
        v1 += items->value;
        // Have to use a different array to avoid clobbering the result above.
        v2 = knapsack_2d(items + 1, len - 1, budget, max_cost, get2);
        if (v2 > v1) {
            *get = 0;
            memcpy(get + 1, get2, (len - 1) * sizeof(int));
            return v2;
        } else {
            *get = 1;
            return v1;
        }
    } else {
        // Can't afford this item.
        *get = 0;
        return knapsack_2d(items + 1, len - 1, budget, max_cost, get + 1);
    }
}

// Recursive helper function for knapsack. Fills the memo array at
// [i][w] with the proper value, and returns the actual value.
// The proper value is given by:
// memo[len-1][w] = items[len-1].value if items[len-1].cost <= w, 0 otherwise.
// memo[i][w] = the better* of memo[i+1][w] and memo[i+1][w - i.w] + i.v
// In the latter case, the value of memo[i][w] is set to ~ of the actual value.
// This is why I say "better", because those memo values may be negative.
int fill_memo(item_t* items, int i, int w, int* memo, int len, int budget) {
    int v1, v2, c;
    if (w <= 0) {
        return 0;
    }
    c = items[i].cost;
    if (i == len - 1) {
        if (w < c) {
            // Can't afford.
            memo[i * budget + w - 1] = 0;
            return 0;
        } else {
            v1 = items[i].value;
            memo[i * budget + w - 1] = ~v1;
            return v1;
        }
    }
    v1 = memo[(i + 1) * budget + w - 1];
    if (v1 == -1) {
        v1 = fill_memo(items, i + 1, w, memo, len, budget);
    } else if (v1 < 0) {
        v1 = ~v1;
    }
    if (w < c) {
        // Can't afford.
        memo[i * budget + w - 1] = v1;
        return v1;
    } else if (w == c) {
        // Separately here so we don't call this function repeatedly
        // with w = 0.
        v2 = items[i].value;
    } else {
        v2 = memo[(i + 1) * budget + w - c - 1];
        if (v2 == -1) {
            v2 = fill_memo(items, i + 1, w - c, memo, len, budget);
        } else if (v2 < 0) {
            v2 = ~v2;
        }
        v2 += items[i].value;
    }
    if (v1 > v2) {
        // taking this item gets precedence for ties.
        memo[i * budget + w - 1] = v1;
        return v1;
    } else {
        memo[i * budget + w - 1] = ~v2;
        return v2;
    }
}

int knapsack(item_t* items, int len, int budget, int* get) {
    // the format of this array is as follows:
    // memo[i][w] = max value obtainable with only items i and forward
    //              and a budget of w+1
    // Additionally, if this value is negative, then the value is ~memo[i][w],
    // and it signifies that the ith item is used. Note that a value of -1
    // would represent using the ith item to get a total value of 0, hence it
    // is not possible.
    // This allows us to walk down the list to fill the get array, starting
    // from memo[0][budget - 1].
    int* memo;
    int i, v, b;
    if (budget <= 0) {
        memset(get, 0, len * sizeof(int));
        return 0;
    }
    memo = malloc(len * budget * sizeof(int));
    memset(memo, -1, len * budget * sizeof(int));
    v = fill_memo(items, 0, budget, memo, len, budget);
    assert(v >= 0);
    b = budget;
    for (i = 0; i < len; ++i) {
        assert(b >= 0);
        if (b == 0) {
            memset(get + i, 0, (len - i) * sizeof(int));
            break;
        }
        if (memo[i * budget + b - 1] < 0) {
            get[i] = 1;
            b -= items[i].cost;
        } else {
            get[i] = 0;
        }
    }
    assert(b >= 0);
    free(memo);
    return v;
}
