/* Copyright (C) 2010 Benjamin S Wolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Benjamin S Wolf <jokeserver@gmail.com>
 */

/*
 * Contains functions for determining distribution of ships to planets.
 */
#ifndef PW_DISTRIBUTION_H
#define PW_DISTRIBUTION_H

#include "orders.h"

typedef struct request {
    int planet_id;
    int turns;
    int ships;
    int value;
} request_t;

// Given a set of requests for ships on planets at various times,
// returns a set of orders to accomplish them as best as possible.
// the length of want is supplied as len, the lengths of budget and alloc
// are each num_planets.
// A request is a desire for a number of ships to arrive at a given
// planet at some point in the future. Therefore, planets with requests
// should have NO BUDGET or else they may send ships away they want to
// keep.
order_set* distribute_ships(request_t* want, int len, int* budget, int* alloc);

#endif /* PW_DISTRIBUTION_H */
