/* Copyright (C) 2010 Benjamin S Wolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Benjamin S Wolf <jokeserver@gmail.com>
 */

/*
 * Implements state-tracking functions and variables.
 */
#include "debug.h"
#include "state.h"
#include "stream.h"
#include "misc.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <string.h>

int numships_p1;
int numships_p2;
int growth_p1;
int growth_p2;

int turn;

int num_planets = 0;
planet* planets;
int* growth;
int* owners;

int** distance;
closest_info** closest;

static int* p_closest;

stream_t* streams = NULL;

// Calculates the distances between planets, memoizes them.
void calc_distance() {
    int i, j;
    int dist;
    double x1, y1, x, y;
    assert(num_planets > 0);
    distance = malloc(num_planets * sizeof(int*));
    for (i = 0; i < num_planets; ++i) {
        distance[i] = malloc(num_planets * sizeof(int));
    }
    for (i = 0; i < num_planets; ++i) {
        x1 = planets[i].x;
        y1 = planets[i].y;
        LOG(DBG_CALC_DISTANCE, "Planet %d\n", i);
        for (j = i; j < num_planets; ++j) {
            if (i == j) {
                distance[i][i] = 0;
            } else {
                x = x1 - planets[j].x;
                y = y1 - planets[j].y;
                dist = (int) ceil(sqrt(x * x + y * y));
                distance[i][j] = dist;
                distance[j][i] = dist;
            }
        }
    }
}

int compare_closest(const void* a, const void* b) {
    return ((closest_info*)a)->distance - ((closest_info*)b)->distance;
}

// Allocates and fills the closest array.
void calc_closest() {
    int i, j;
    assert(distance != NULL);
    closest = malloc(num_planets * sizeof(closest_info*));
    p_closest = malloc(num_planets * sizeof(int));
    memset(p_closest, -1, num_planets * sizeof(int));
    for (i = 0; i < num_planets; ++i) {
        LOG(DBG_CALC_CLOSEST, "Planet %d\n", i);
        closest[i] = malloc(num_planets * sizeof(closest_info));
        // Fill first then sort
        for (j = 0; j < num_planets; ++j) {
            closest[i][j].planet_id = j;
            closest[i][j].distance = distance[i][j];
        }
        qsort(closest[i], num_planets, sizeof(closest_info), &compare_closest);
    }
}

// Finds the closest player to a given planet, in a lazily memoized fashion.
// If both players are equidistant, returns 0.
int closest_player(int planet) {
    int d = 0, p = 0;
    closest_info* cl;
    if (p_closest[planet] > -1) {
        return p_closest[planet];
    }
    for (cl = closest[planet] + 1; cl < closest[planet] + num_planets; ++cl) {
        if (d > 0) {
            if (cl->distance > d) {
                p_closest[planet] = p;
                return p;
            }
            if (STREAM_MIN_INDEX(streams[cl->planet_id]).owner == 3 - p) {
                p_closest[planet] = 0;
                return 0;
            }
        } else {
            if (STREAM_MIN_INDEX(streams[cl->planet_id]).owner != 0) {
                p = STREAM_MIN_INDEX(streams[cl->planet_id]).owner;
                d = cl->distance;
                // continue in case there's an equidistant other player
            }
        }
    }
}

void copy_growth() {
    int i;
    assert(num_planets > 0);
    growth = malloc(num_planets * sizeof(int));
    for (i = 0; i < num_planets; ++i) {
        growth[i] = planets[i].growth;
    }
}

void copy_owners() {
    int i;
    assert(num_planets > 0);
    owners = malloc(num_planets * sizeof(int));
    for (i = 0; i < num_planets; ++i) {
        owners[i] = planets[i].owner;
    } 
}

void init_streams() {
    int i, j, max_dist;
    int ships, owner, g;
    assert(closest != NULL);
    streams = malloc(num_planets * sizeof(stream_t));
    for (i = 0; i < num_planets; ++i) {
        max_dist = closest[i][num_planets - 1].distance + 1;
        LOG(DBG_ALLOC_STREAMS, "Planet %d allocated stream of length %d.\n",
                               i, max_dist);
        streams[i].array = calloc(max_dist, sizeof(stream_buffer_t));
        streams[i].length = max_dist;
        streams[i].cur_index = 0;
        ships = planets[i].ships;
        owner = planets[i].owner;
        g = planets[i].growth;
        // init buffers
        for (j = 0; j < max_dist; ++j, ships += (owner ? g : 0)) {
            streams[i].array[j].owner = owner;
            streams[i].array[j].ships = ships;
        }
        LOG_STREAM(ALLOC_STREAMS, streams[i], i);
    }
}

void init() {
    planet p;
    char c, d;
    int owner, num_ships, g, src, dest, a;
    double x, y;
    numships_p1 = 0;
    numships_p2 = 0;
    growth_p1 = 0;
    growth_p2 = 0;
    num_planets = 0;
    planets = malloc(64 * sizeof(planet));
    while (1) {
        scanf(" %c%c", &c, &d);
        switch (c) {
        case 'P':
            a = scanf(" %lf %lf %d %d %d", &x, &y,
                      &owner, &num_ships, &g);
            LOG(DBG_READ_PLANETS,
                "Planet %d: (%lf, %lf) owner:%d ships:%d growth:%d\n",
                num_planets, x, y, owner, num_ships, g);
            assert(a == 5);
            if (owner == 1) {
                numships_p1 += num_ships;
                growth_p1 += g;
            } else if (owner == 2) {
                numships_p2 += num_ships;
                growth_p2 += g;
            }
            p.x = x;
            p.y = y;
            p.owner = owner;
            p.ships = num_ships;
            p.growth = g;
            planets[num_planets++] = p;
            break;
        /*case 'F':
            if (streams != NULL) {
                // First fleet encountered, all planets done reading
                // Most games should have no fleets anyway on turn 1.
            }
            a = scanf(" %d %d %d %d %d %d", &owner, &num_ships,
                      &src, &dest, &tt, &tr);
            assert(a == 6);
            if (owner == 1) {
                numships_p1 += num_ships;
            } else if (owner == 2) {
                numships_p2 += num_ships;
            }
            
            break;*/
        case 'g':
            turn = 1;
            LOG(DBG_TURN_START, "Turn 1 starting.\n");
            // initialize all data
            calc_distance();
            calc_closest();
            copy_growth();
            copy_owners();
            init_streams();
            LOG(DBG_TURN_START, "Initialization complete.\n");
            LOG(DBG_GAME_STATS, "P1 ships: %d growth: %d P2 ships: %d growth: %d\n",
                                numships_p1, growth_p1, numships_p2, growth_p2);
            LOG(DBG_GAME_STATS, "Distance between starting planets: %d\n",
                                distance[1][2]);
            LOG(DBG_GAME_STATS, "Distance from starting planet to center: %d\n",
                                distance[1][0]);
            LOG(DBG_GAME_STATS, "Center planet growth: %d\n", growth[0]);
            return;
        case '#':
            // skip to EOL
            while (d != '\n') {
                scanf("%c", &d);
            }
            break;
        }
    }
}

void iter_streams() {
    int i;
    int ships, owner;
    for (i = 0; i < num_planets; ++i) {
        ships = STREAM_MAX_INDEX(streams[i]).ships;
        owner = STREAM_MAX_INDEX(streams[i]).owner;
        if (owner) {
            ships += growth[i];
        }
        STREAM_MIN_INDEX(streams[i]).ships = ships;
        STREAM_MIN_INDEX(streams[i]).owner = owner;
        ITERATE_STREAM(streams[i]);
        STREAM_MIN_INDEX(streams[i]).inc_ships_p1 = 0;
        STREAM_MIN_INDEX(streams[i]).inc_ships_p2 = 0;
    }
}

// smooth the stream number p from position n > 0.
// ships = number of ships on the planet on the previous position
// owner = owner on the previous position
void smooth_stream_from_i(int p, int n, int ships, int owner) {
    int p1, p2;
    int g = growth[p];
    stream_buffer_t* buf;
    LOG_STREAM(SMOOTH_STREAM, streams[p], p);
    LOG(DBG_SMOOTH_START, "Smoothing stream %d from %d\n", p, n);
    assert(ships >= 0);
    FOR_STREAM_FROM(buf, n, streams[p]) {
        p1 = buf->inc_ships_p1;
        p2 = buf->inc_ships_p2;
        assert(p1 >= 0);
        assert(p2 >= 0);
        LOG(DBG_SMOOTH_STEP, "Owner %d, Ships %d, P1 %d, P2 %d\n",
            owner, ships, p1, p2);
        if (owner) {
            ships += g;
        }
        switch (owner) {
        case 0:
            // three way case
            // note that if p1 == 0 and/or p2 == 0, this is still correct
            if (p1 >= p2) {
                if (p1 > ships) {
                    LOG(DBG_SMOOTH_STEP, "ships: %d p1: %d\n", ships, p1);
                    ships = p1 - max(p2, ships);
                    // since p1 could equal p2, owner might still be p0
                    owner = (ships > 0) ? 1 : 0;
                } else {
                    ships -= p1;
                }
            } else {
                if (p2 > ships) {
                    ships = p2 - max(p1, ships);
                    owner = 2; // p2 strictly greater than p1
                } else {
                    // if p2 == ships, this becomes 0 but still p0's
                    ships -= p2;
                }
            }
            break;
        case 1:
            ships += p1;
            if (p2 > ships) {
                ships = p2 - ships;
                owner = 2;
            } else {
                ships -= p2;
            }
            break;
        case 2:
            ships += p2;
            if (p1 > ships) {
                ships = p1 - ships;
                owner = 1;
            } else {
                ships -= p1;
            }
            break;
        }
        LOG(DBG_SMOOTH_STEP, "Result: Owner %d Ships %d\n", owner, ships);
        assert(ships >= 0);
        buf->ships = ships;
        buf->owner = owner;
    }
}

void smooth_stream(int p) {
    smooth_stream_from_i(p, 1, STREAM_MIN_INDEX(streams[p]).ships,
                         STREAM_MIN_INDEX(streams[p]).owner);
}

void smooth_stream_from(int p, int n) {
    smooth_stream_from_i(p, n, STREAM_INDEX(streams[p], n - 1).ships,
                         STREAM_INDEX(streams[p], n - 1).owner);
}

void refresh_state() {
    char c, d;
    int owner, num_ships, src, dest, tr, tt;
    int p = 0, a, g;
    int refresh_time[num_planets];
    memset(refresh_time, -1, num_planets * sizeof(int));
    memset(p_closest, -1, num_planets * sizeof(int));
    iter_streams();
    // easier to recalc than to keep track in eg iter_stream
    numships_p1 = 0;
    numships_p2 = 0;
    growth_p1 = 0;
    growth_p2 = 0;
    while (1) {
        scanf(" %c%c", &c, &d);
        switch (c) {
        case 'P':
            a = scanf(" %*f %*f %d %d %d", &owner, &num_ships, &g);
            assert(a == 3);
            owners[p] = owner;
            if (owner == 1) {
                numships_p1 += num_ships;
                growth_p1 += g; //growth[p];
            } else if (owner == 2) {
                numships_p2 += num_ships;
                growth_p2 += g; //growth[p];
            }
            if (num_ships != STREAM_MIN_INDEX(streams[p]).ships) {
                refresh_time[p] = 0;
                STREAM_MIN_INDEX(streams[p]).ships = num_ships;
            }
            if (owner != STREAM_MIN_INDEX(streams[p]).owner) {
                refresh_time[p] = 0;
                STREAM_MIN_INDEX(streams[p]).owner = owner;
            }
            ++p;
            break;
        case 'F':
            a = scanf(" %d %d %d %d %d %d", &owner, &num_ships, &src,
                      &dest, &tt, &tr);
            assert(a == 6);
            switch(owner) {
            case 1:
                numships_p1 += num_ships;
                break;
            case 2:
                numships_p2 += num_ships;
                break;
            }
            if (tt - tr == 1) {
                // New fleet!
                switch(owner) {
                case 1:
                    STREAM_INDEX(streams[dest], tr).inc_ships_p1 += num_ships;
                    break;
                case 2:
                    STREAM_INDEX(streams[dest], tr).inc_ships_p2 += num_ships;
                    break;
                }
                if (refresh_time[dest] == -1) {
                    refresh_time[dest] = tr;
                } else {
                    refresh_time[dest] = min(refresh_time[dest], tr);
                }
            }
            break;
        case 'g':
            ++turn;
            LOG(DBG_TURN_START, "Turn %d starting.\n", turn);
            LOG(DBG_GAME_STATS, "P1 ships: %d growth: %d P2 ships: %d growth: %d\n",
                                numships_p1, growth_p1, numships_p2, growth_p2);
            // done reading input for the turn
            // refresh all streams as necessary
            for (p = 0; p < num_planets; ++p) {
                if (refresh_time[p] >= 0) {
                    if (refresh_time[p] == 0) {
                        smooth_stream(p);
                    } else {
                        smooth_stream_from(p, refresh_time[p]);
                    }
                }
            }
            // reset this
            memset(p_closest, -1, num_planets * sizeof(int));
            return;
        case '#':
            // skip to EOL
            while (d != '\n') {
                scanf("%c", &d);
            }
            break;
        }
    }
}
