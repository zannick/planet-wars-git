/* Copyright (C) 2010 Benjamin S Wolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Benjamin S Wolf <jokeserver@gmail.com>
 */

/*
 * Contains structs and functions to handle orders.
 */
#ifndef PW_ORDERS_H
#define PW_ORDERS_H

#include "debug.h"

typedef struct orders {
    int from;
    int to;
    int ships;
} orders_t;

typedef struct order_set {
    int len;
    orders_t* orders; // the start of a contiguous block of len structs
} order_set;

// Order to be sent in the future
typedef struct future_orders {
    int turns;
    int from;
    int to;
    int ships;
} future_orders_t;

// Create an empty set of len orders.
inline order_set* alloc_order_set(int len);

// Combine multiple order_sets.
inline order_set* merge_order_sets(order_set** sets, int len);

// Output the order
inline void issue_order(int from, int to, int numships);

inline void destroy_order_set(order_set* set);

inline void issue_order_t(orders_t* order);

// Output all orders.
// Doesn't update state.
inline void process_orders(order_set* set);

inline void end_turn();

#ifdef DEBUG
#define LOG_ORDERS(flag, set) \
        { orders_t* o; \
          if (set == NULL) { \
              LOG(DBG_ ## flag, "No orders.\n"); \
          } else { \
              LOG(DBG_ ## flag, "Order set (%d orders): [from, to, ships]\n", \
                  (set)->len); \
              for (o = (set)->orders; o < (set)->orders + (set)->len; ++o) { \
                  LOG(DBG_ ## flag, "  %d %d %d\n", o->from, o->to, o->ships); \
              } \
          } \
        }
#else
#define LOG_ORDERS(flag, set)
#endif

#endif
