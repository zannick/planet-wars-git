/* Copyright (C) 2010 Benjamin S Wolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Benjamin S Wolf <jokeserver@gmail.com>
 */

#include "debug.h"
#include "aggression.h"
#include "knapsack.h"
#include "misc.h"
#include "orders.h"
#include "state.h"
#include "stream.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

void turn_one() {
    /* On the first turn, both players have one planet, 100 ships, and 5 growth.
     * We want to maximize the difference in growth as soon as possible.
     * But we have to be wary of the "Rage" strategy, where the enemy
     * launches all their ships directly at our home planet on turn 1.
     * We can do this in one of two ways: send only a number of ships such that
     * by the time the enemy fleet arrives we have at least 100; or send a number
     * of ships to take their home planet simultaneously. The latter may even be
     * a good strategy if the enemy launches all their ships (though not
     * necessarily all at us). However, they may be conquering nearby planets,
     * from which they can immediately return to defend their home planet.
     * *Gaining their home planet is strong, but less important (and less likely)
     * than the guaranteed growth from the nearby planets.*
     */
    int dist = distance[1][2];
    // this is both the number of ships i can send defensively, and 1 less than
    // the number of ships i have to send to take his planet if he vacates completely
    int base_max = 5 * dist;
    // if this is 1 then my planet is 1, if it's 2, then my planet is 2.
    int p = owners[1];
    int q = 3 - p;
    int i, w, r;
    item2_t want[num_planets];
    int get[num_planets];
    int orderlen, orderi;
    int growth_def = 0, growth_ab, growth_off = 0;
    int total_rebate;
    int total_cost;
    order_set* orders_def = NULL;
    order_set* orders_ab = NULL;
    order_set* orders_off = NULL;
    closest_info* c;
    assert(p == 1 || p == 2);
    for (w = 0, c = closest[p] + 1; ; ++c) {
        i = c->planet_id;
        if (i == q) {
            if (w == 0) {
                LOG(DBG_TURN_ONE, "No planets closer than enemy! Allowing all.\n");
                continue;
            }
            break;
        }
        if (i == 0 || distance[q][i] < distance[p][i]) {
            continue;
        }
        r = 1 + growth[i] * (dist - 2 * c->distance);
        want[w].id = i;
        want[w].value = growth[i];
        want[w].cost = streams[i].array[0].ships + 1;
        want[w].rebate = max(r, 0);
        ++w;
    }

    LOG(DBG_TURN_ONE, "Examining %d planets closer than enemy.\n", w);
    // Examine the abandon strategy first so that we can use
    // an estimate for rebate numbers.
    LOG(DBG_TURN_ONE, "Abandon strategy, budget = 100.\n");
    // Assume we lose our home planet. That's a swing of 10 since we lose
    // it to our opponent. (This means we'll be very reluctant to follow this
    // strategy.)
    growth_ab = knapsack_2d(want, w, 100, 100, get) - 10;
    LOG(DBG_TURN_ONE, "Max growth obtainable (-10) (ab. strategy) is %d.\n",
                      growth_ab);
    total_rebate = 0;
    total_cost = 0;
    for (i = 0, orderlen = 0; i < w; ++i) {
        if (get[i]) {
            ++orderlen;
            total_rebate += want[i].rebate;
            total_cost += want[i].cost;
        }
    }
    orders_ab = alloc_order_set(orderlen);

    for (i = 0, orderi = 0; i < w; ++i) {
        if (get[i]) {
            LOG(DBG_TURN_ONE,
                "Send %d ships to planet %d (%d away, %d growth), rebate of %d.\n",
                want[i].cost, want[i].id, distance[p][want[i].id],
                want[i].value, want[i].rebate);
            orders_ab->orders[orderi].from = p;
            orders_ab->orders[orderi].to = want[i].id;
            orders_ab->orders[orderi].ships = want[i].cost;
            ++orderi;
        }
    }
    
    // silly to calc this again if base_max >= 100.
    if (base_max >= 100) {
        LOG(DBG_TURN_ONE, "Opponent too far for offense; defense same as abandon.\n");
        process_orders(orders_ab);
        end_turn();
        destroy_order_set(orders_ab);
        return;
    }
    
    // 100 - total_cost + total_rebate > 100 - base_max
    if (total_cost - total_rebate < base_max) {
        // The number of ships we'll have left is better than what we'll
        // need, therefore this will be the best defensive strategy, too.
        LOG(DBG_TURN_ONE, "Best abandon strategy = defensive strategy.\n");
        growth_def = growth_ab + 10;
        growth_ab = 0;
        orders_def = orders_ab;
        orders_ab = NULL;
    } else {
        LOG(DBG_TURN_ONE, "Defensive strategy, budget = %d.\n", base_max);
        growth_def = knapsack_2d(want, w, base_max, 100, get);
        LOG(DBG_TURN_ONE, "Max growth obtainable (def. opening) is %d.\n",
                          growth_def);
        for (i = 0, orderlen = 0; i < w; ++i) {
            if (get[i]) {
                ++orderlen;
            }
        }
        orders_def = alloc_order_set(orderlen);

        for (i = 0, orderi = 0; i < w; ++i) {
            if (get[i]) {
                LOG(DBG_TURN_ONE,
                    "Send %d ships to planet %d (%d away, %d growth),"
                    " rebate of %d.\n",
                    want[i].cost, want[i].id, distance[p][want[i].id],
                    want[i].value, want[i].rebate);
                orders_def->orders[orderi].from = p;
                orders_def->orders[orderi].to = want[i].id;
                orders_def->orders[orderi].ships = want[i].cost;
                ++orderi;
            }
        }
    }

    // won't do any good to think about being offensive if we can't conquer him
    // by launching now
    // essentially this is 1 + the number of ships he'll gain while we travel
    // + the number of ships he could get back (an underestimate, likely)
    if (base_max + 1 + total_rebate < 100) {
        // Use same want list but pretend we're going to attempt to
        // take enemy planet, hoping he abandons it completely. If he does,
        // minimum necessary to take it is base_max + 1. However, he may
        // have rebates like we considered!
        r = 99 - base_max - total_rebate;
        LOG(DBG_TURN_ONE, "Offensive strategy. %d ships to him, scatter rest.\n",
                          base_max + 1 + total_rebate);
        // Assume we trade home planets, or no change in them.
        growth_off = knapsack_2d(want, w, r, r, get);
        LOG(DBG_TURN_ONE, "Max growth obtainable (def. opening): %d.\n", growth_def);
        for (i = 0, orderlen = 0; i < w; ++i) {
            if (get[i]) {
                ++orderlen;
            }
        }
        orders_off = alloc_order_set(orderlen + 1);
        orders_off->orders[0].from = p;
        orders_off->orders[0].to = q;
        orders_off->orders[0].ships = 100 - r;
        for (i = 0, orderi = 1; i < w; ++i) {
            if (get[i]) {
                LOG(DBG_TURN_ONE,
                    "Send %d ships to planet %d (%d away, %d growth), rebate %d.\n",
                    want[i].cost, want[i].id, distance[p][want[i].id],
                    want[i].value, want[i].rebate);
                orders_off->orders[orderi].from = p;
                orders_off->orders[orderi].to = want[i].id;
                orders_off->orders[orderi].ships = want[i].cost;
                ++orderi;
            }
        }
    }
    // Offense should happen only if it is strictly better than defense.
    // Abandon should happen only if it is strictly better than off/def.
    // That is, defense wins all ties, offense wins ties with abandon.
    if (growth_ab > growth_off && growth_ab > growth_def) {
        LOG(DBG_TURN_ONE, "Selecting abandon strategy.\n");
        process_orders(orders_ab);
    } else if (growth_off > growth_def) {
        LOG(DBG_TURN_ONE, "Selecting offensive strategy.\n");
        process_orders(orders_off);
    } else if (growth_def > 0) {
        LOG(DBG_TURN_ONE, "Selecting defensive strategy.\n");
        process_orders(orders_def);
    }
    end_turn();

    destroy_order_set(orders_ab);
    destroy_order_set(orders_off);
    destroy_order_set(orders_def);
}

void check_aggression() {
    int dist = 64, i;
    closest_info* cl;
    // Measure tension
    for (i = 0; i < num_planets; ++i) {
        if (!frontline[i]) {
            continue;
        }
        for (cl = closest[i] + 1; cl < closest[i] + num_planets; ++cl) {
            if (STREAM_MIN_INDEX(streams[cl->planet_id]).owner
                    == 3 - STREAM_MIN_INDEX(streams[i]).owner) {
                dist = min(dist, cl->distance);
            }
        }
    }

    if (numships_p1 > 3 * numships_p2 / 2 || numships_p1 > 99 + numships_p2) {
        if (numships_p1 > 3 * numships_p2 || numships_p1 > 199 + numships_p2) {
            if (growth_p1 > growth_p2) {
                set_aggression_level(AGG_DESTROY);
            } else {
                set_aggression_level(AGG_ATTACK);
            }
        } else {
            if (growth_p1 > growth_p2) {
                set_aggression_level(AGG_ATTACK);
            } else if (dist < 11) {
                set_aggression_level(AGG_ATTACK);
            } else if (dist > 16) {
                set_aggression_level(AGG_CONQUER);
            } else {
                set_aggression_level(AGG_CAUTION);
            }
        }
    } else if (numships_p1 > numships_p2) {
        if (growth_p1 > growth_p2 + 10) {
            set_aggression_level(AGG_ATTACK);
        } else if (growth_p1 > growth_p2) {
            if (dist < 11) {
                set_aggression_level(AGG_DEFEND);
            } else {
                set_aggression_level(AGG_CAUTION);
            }
        } else {
            if (dist < 11) {
                set_aggression_level(AGG_ATTACK);
            } else if (dist > 16) {
                set_aggression_level(AGG_CONQUER);
            } else {
                set_aggression_level(AGG_CAUTION);
            }
        }
    } else {
        if (growth_p1 > growth_p2) {
            if (dist < 11) {
                set_aggression_level(AGG_DEFEND);
            } else {
                set_aggression_level(AGG_CAUTION);
            }
        } else {
            if (numships_p1 > numships_p2 - 30) {
                set_aggression_level(AGG_ATTACK);
            } else {
                set_aggression_level(AGG_DEFEND);
            }
        }
    }
}

void do_turn() {
    int budget[num_planets];
    int alloc[num_planets];
    int i;
    order_set* def;
    order_set* sniper;
    order_set* conq = NULL;
    order_set* atk;
    order_set* reinf;
    stream_buffer_t* buf;
    memset(alloc, 0, num_planets * sizeof(int));
    find_frontline(frontline);
    check_aggression();
    for (i = 0; i < num_planets; ++i) {
        if (STREAM_MIN_INDEX(streams[i]).owner == 1) {
            budget[i] = STREAM_MIN_INDEX(streams[i]).ships;
            FOR_STREAM_FROM(buf, 1, streams[i]) {
                if (buf->owner != 1) {
                    alloc[i] = budget[i];
                    break;
                }
                if (buf->ships < budget[i]) {
                    budget[i] = buf->ships;
                }
            }
        } else {
            budget[i] = 0;
        }
    }
    switch(aggression) {
    case AGG_DEFEND:
        def = defense(budget, alloc);
        sniper = snipe(budget, alloc);
        atk = attack(budget, alloc);
        if (!(turn > 50 && growth_p1 > 35)) {
            conq = conquer(budget, alloc);
        }
        break;
    case AGG_CAUTION:
        sniper = snipe(budget, alloc);
        def = defense(budget, alloc);
        atk = attack(budget, alloc);
        if (!(turn > 50 && growth_p1 > 35)) {
            conq = conquer(budget, alloc);
        }
        break;
    case AGG_CONQUER:
        sniper = snipe(budget, alloc);
        if (!(turn > 50 && growth_p1 > 35)) {
            conq = conquer(budget, alloc);
        }
        def = defense(budget, alloc);
        atk = attack(budget, alloc);
        break;
    case AGG_ATTACK:
        atk = attack(budget, alloc);
        def = defense(budget, alloc);
        sniper = snipe(budget, alloc);
        if (!(turn > 50 && growth_p1 > 35)) {
            conq = conquer(budget, alloc);
        }
        break;
    case AGG_DESTROY:
        atk = attack(budget, alloc);
        sniper = snipe(budget, alloc);
        def = defense(budget, alloc);
        if (!(turn > 50 && growth_p1 > 35)) {
            conq = conquer(budget, alloc);
        }
        break;
    }
    reinf = reinforce(budget, alloc, atk, def, conq, sniper);
    process_orders(def);
    process_orders(sniper);
    process_orders(conq);
    process_orders(atk);
    process_orders(reinf);

    end_turn();
    destroy_order_set(def);
    destroy_order_set(sniper);
    destroy_order_set(conq);
    destroy_order_set(atk);
    destroy_order_set(reinf);
}

int main() {
    struct timeval start, finish;
    OPEN_LOG("mybot.log", "w");
    gettimeofday(&start, NULL);
    init();
    turn_one();
    gettimeofday(&finish, NULL);
    while(1) {
        LOG(DBG_TURN_END, "Turn %d took %.2lf secs.\n",
                          turn, finish.tv_sec - start.tv_sec +
                            ((double)(finish.tv_usec - start.tv_usec))/1000000);
        gettimeofday(&start, NULL);
        refresh_state();
        do_turn();
        gettimeofday(&finish, NULL);
    }
    CLOSE_LOG();
    return 0;
}
