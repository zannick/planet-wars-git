/* MyBot.cc, a simple copycat bot for the Planet Wars AI Challenge
 * Copyright (C) 2010 Benjamin S Wolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Benjamin S Wolf <jokeserver@gmail.com>
 */

#include <iostream>
#include <vector>
#include "PlanetWars.h"

using std::vector;

inline int mirror(int x) {
    if (x % 2 == 0) {
        return x - 1;
    } else {
        return x + 1;
    }
}

inline int min(int a, int b) {
    return (b < a) ? b : a;
}

void DoTurn( PlanetWars& pw) {
  // For every fleet the opponent launched last turn,
  // launch a fleet from the mirror source planet to the mirror dest planet
  // (mirror = 1<->2 3<->4 etc.)
  vector<Fleet> fp2 = pw.EnemyFleets();
  int num_planets_ = pw.NumPlanets();
  vector< vector< int > > orders(num_planets_, vector<int>(num_planets_,  0));
  vector< int > orders_for(num_planets_, 0);
  for (vector<Fleet>::iterator i = fp2.begin(); i != fp2.end(); ++i) {
    if (i->TotalTripLength() - i->TurnsRemaining() == 1) {
      int src   = i->SourcePlanet(),
          dest  = i->DestinationPlanet(),
          ships = i->NumShips();
      if (src) {
        int s2 = src ? mirror(src) : 0, // if src is 0, we can't launch anyway
            d2 = dest ? mirror(dest) : 0;
        if (pw.planets_[s2].Owner() == 1) {
          orders[s2][d2] += min(ships, pw.planets_[s2].NumShips());
          orders_for[s2] += 1;
        }
      }
    }
  }
  // Go through all the orders per src, dest pair
  for (int i = 0; i < num_planets_; ++i) {
    if (orders_for[i] == 0) {
        continue;
    }
    for (int j = 0; j < num_planets_; ++j) {
      if (i == j || orders[i][j] == 0) {
        continue;
      }
      int ships = min(orders[i][j] + (pw.planets_[i].GrowthRate() / orders_for[i]),
                      pw.planets_[i].NumShips());
      if (ships > 0) {
        pw.IssueOrder(i, j, ships);
        pw.planets_[i].RemoveShips(ships);
      }
    }
  }
}

// This is just the main game loop that takes care of communicating with the
// game engine for you. You don't have to understand or change the code below.
int main(int argc, char *argv[]) {
  std::string current_line;
  std::string map_data;
  while (true) {
    int c = std::cin.get();
    current_line += (char)c;
    if (c == '\n') {
      if (current_line.length() >= 2 && current_line.substr(0, 2) == "go") {
        PlanetWars pw(map_data);
        map_data = "";
        DoTurn(pw);
	pw.FinishTurn();
      } else {
        map_data += current_line;
      }
      current_line = "";
    }
  }
  return 0;
}
