Planet Wars Utilities
by Zannick

This folder contains several utility scripts for testing your
Planet Wars bot. They assume that they exist in a folder with the
following folders provided by a starter package:
maps/ tools/

The tcpwar script assumes you've downloaded tcp.c from
http://www.benzedrine.cx/planetwars
and compiled it to an executable named 'tcp'.

Utility scripts in this folder:

war: ./war <bot1> <bot2> <map number> [--nogui].

     Run a game using the PlayGame.jar included in your starter package.
     The timeout is preset to 1000 milliseconds and the turn limit is 300.
     These can be changed in the script itself.

     Each bot is a filename. The script is currently set up to accept jars
     and executables. You don't need to put the "java -jar" or the "./" in
     front, the script will do that for you.

     The map number is just a number. "maps/map" and ".txt" are added for you.

     Add the flag --nogui to prevent the script from passing the result
     of the game to the Java visualizer included in your starter package.

war2: ./war2 <map number> <bot1> <bot2> [bot3, ...] [--gui]

      Run a game using a modified version of the official game engine.
      The timeout is preset to 1000 milliseconds and the turn limit is 200.
      These can be changed in the script itself.

      As in war, each bot is a filename, either a jar or an executable.
      You don't need to include the "java -jar" or the "./" as the script
      will add those for you.

      You can include any number of bots this way. However, the map must
      have more planets than bots. Each bot numbered n will start with
      Planet ID n (so players 1, 2, 3 get planets 1, 2, 3).

      The map number is just a number.

      Add the flag --gui to record the playback from the game to playback.log
      and feed that to the Java visualizer included in your starter package.

tcpwar: ./tcpwar <username> [-p <password>] <bot> <logfile> [t [n]]

        Run a game on the tcp client provided by dhartmei at
        http://www.benzedrine.cx/planetwars

        The bot must be an executable. "./" is appended by the script.

        The logfile will contain everything output by tcp, while the script
        will refrain from pasting (what it believes to be) the input and output
        to the bot itself. This can be modified via the ignorelines regex in
        the script. You can specify /dev/null as the logfile.

        If t is provided, it is a time to sleep between games before rerunning
        tcp to play another game. If n is also provided, it is the number of
        games to play this way. If n is not provided, the script will loop
        until it is killed. This may create large logfiles if you are not
        careful!

gauntlet: ./gauntlet <m> <n> <bot> <opp1> [opp2, ...]

          Run your bot in a gauntlet series of matches against each opponent
          given, on the maps numbered m through n (inclusive). The gauntlet
          will end when all opponents have been played on all maps, or your
          bot fails, issues a bad order, or times out.

          The bot and opponents should be executables, python scripts, or
          jars. "./", "python", or "java -jar" will be appended as appropriate.

          Each game is preset to be 1000 millisecond turns, and 200 turns max.

          The outcome of all matches against each opponent will be aggregated
          into W-L-D (win-loss-draw) format, with a win percentage, a list
          of games lost, and a graphical view, such as ++++-+=++*-, where
          + indicates a win, - a loss, = a tie, and * a win due to the opponent
          timing out, failing, or giving a bad order.

          The overall result will similarly have W-L-D and win percent, but
          will not include a list of games lost, and the graphical view will
          simply be the other graphs appended together with "|" separating
          them.

You do not need to worry about engine.py or user_sadbox.py, but they are
necessary for the war2 and gauntlet scripts.
